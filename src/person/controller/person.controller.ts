import { Controller, Get, Post, Body, Param, Put } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AddManagerDepart, PersonDTO } from '../dto/create-person.dto';
import { Person } from '../person.entity';
import { PersonService } from '../service/person.service';

@Controller('api/persons')
export class PersonController {
  constructor(
    private personService: PersonService,
    // @InjectRepository(Person) private personRepo: Repository<Person>,
  ) { }

  // @Get()
  // getPersons(){
  //      return this.personService.findAll();
  //     // return this.personRepo.find();
  // }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.personService.findOne(id);
  }


  @Post()
  create(@Body() body: PersonDTO) {
    return this.personService.create(body);
  }

  @Get()
  getStatisticalPerson() {
    return this.personService.getStatistic();
  }

  @Get('statistic/:id')
  getStatisticalPersonDepartment(@Param('id') id: number) {
    return this.personService.getStatisticalOneDepartment(id);
  }

  @Put('manager')
  addNewManage(@Body() body: AddManagerDepart) {
    return this.personService.addManageDepartment(body)
  }

  @Put('department')
  addDepartmentToUser(@Body() body: AddManagerDepart) {
    return this.personService.addPersonToDepartment(body)
  }

}
