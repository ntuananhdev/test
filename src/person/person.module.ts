import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DepartmentModule } from 'src/department/department.module';
import { PersonController } from './controller/person.controller';
import { Person } from './person.entity';
import { PersonService } from './service/person.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Person]),
    DepartmentModule
  ],
  providers: [PersonService],
  controllers: [PersonController],
  exports: [PersonService]
})
export class PersonModule { }
