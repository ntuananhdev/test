import { Person } from 'src/person/person.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn } from 'typeorm';


@Entity()
export class Department {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ length: 500 })
  description: string;

  @OneToMany(type => Person, persons => persons.department)
  persons: Person[];

  @OneToOne(() => Person)
  @JoinColumn()
  managerDepart: Person;
}
