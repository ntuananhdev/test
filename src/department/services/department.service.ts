import { Injectable } from '@nestjs/common';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Department } from '../department.entity';
import { DepartmentDTO } from '../dto/create-department.dto';
import { DepartmentRepository } from '../repository/department.repository';

@Injectable()
export class DepartmentService {
  query: any;
  constructor(
    private departmentRepo: DepartmentRepository,
  ) { }
  findAll() {
    return this.departmentRepo.find()
  }

  findOne(id: number) {
    return this.departmentRepo.findOne(id)
  }
  async getDepartById(id: number) {
    const aa = await this.departmentRepo.query('select * from department where id = ?' + id)
    console.log(aa)
    return aa
  }
  create(body: DepartmentDTO) {
    const newDepartment = new Department();
    newDepartment.name = body.name;
    newDepartment.description = body.description;
    return this.departmentRepo.save(newDepartment);
  }
  updateDepartment(department: Department) {
    return this.departmentRepo.update(department.id, department)
  }
  async getAllDepartment() {
    return await this.departmentRepo.query('select * from department')
  }
  add(body: Department) {
    return this.departmentRepo.save(body);
  }
  async getOneDepartment(id: number) {
    return await this.departmentRepo.findOne(
      {
        relations: ["managerDepart"],
        where: { id: id }
      }
    )
  }
  async getOneDepartmentByName(name: string) {
    return await this.departmentRepo.findOne(
      {
        relations: ["managerDepart"],
        where: { name: name }
      }
    )
  }
}
