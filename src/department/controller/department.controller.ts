import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { DepartmentDTO } from '../dto/create-department.dto';
import { DepartmentService } from '../services/department.service';

@Controller('/api/department')
export class DepartmentController {
  constructor(
    private departmentService: DepartmentService
  ) { }

  @Get()
  getDepartment() {
    return this.departmentService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.departmentService.findOne(id);
  }

  @Post()
  create(@Body() body: DepartmentDTO) {
    return this.departmentService.create(body);
  }
}
