import { Controller, Get } from '@nestjs/common';
import { Department } from './department/department.entity';
import { DepartmentService } from './department/services/department.service';
import { Person } from './person/person.entity';
import { PersonService } from './person/service/person.service';

@Controller()
export class AppController {
  constructor(
    private personService: PersonService,
    private departmentService: DepartmentService
  ) { }

  @Get()
  getHello(): string {
    return "hello word";
  }
  @Get('/add-data')
  async addData(): Promise<string> {
    const person = await this.personService.findAll();
    const departments = await this.departmentService.findAll();
    if (person.length > 0 || departments.length > 0) {
      console.log("yes")
    } else {
      let department = new Department()
      let department2 = new Department()
      let department3 = new Department()
      department.name = 'phòng nhân sự'
      department.description = 'phòng nhân sự'
      await this.departmentService.add(department)
      department2.name = 'phòng IT'
      department2.description = 'IT'
      await this.departmentService.add(department2)
      department3.name = 'phòng quản lý'
      department3.description = 'phòng quản lý'
      await this.departmentService.add(department3)
      console.log("chưa có data")
      const nhanSu = await this.departmentService.getOneDepartmentByName("phòng nhân sự")
      const IT = await this.departmentService.getOneDepartmentByName("phòng IT")
      const quanLy = await this.departmentService.getOneDepartmentByName("phòng quản lý")
      let person = new Person()
      person.name = 'trung'
      person.age = 22
      person.gender = 'male'
      person.mail = 'trung@gmail.com'
      person.department = nhanSu
      await this.personService.add(person)
      console.log("ok")
      let person2 = new Person()
      person2.name = 'hung'
      person2.age = 22
      person2.gender = 'male'
      person2.mail = 'trung@gmail.com'
      person2.department = nhanSu
      await this.personService.add(person2)
      let person3 = new Person()
      person3.name = 'Anh'
      person3.age = 22
      person3.gender = 'male'
      person3.mail = 'trung@gmail.com'
      person3.department = nhanSu
      await this.personService.add(person3)
      let person4 = new Person()
      person4.name = 'Ngọc'
      person4.age = 22
      person4.gender = 'female'
      person4.mail = 'trung@gmail.com'
      person4.department = nhanSu
      await this.personService.add(person4)

    }

    return "Dữ liệu ok";
  }

}
